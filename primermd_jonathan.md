# Chemical elements

----

## **An *ordered* list of alkali metals**

1. Lithium (3)
2. Sodium (11)
3. Potassium (19)

## **An *unordered* list** of halogens

- Fluorine (9)
- Chlorine (17)
- Bromine (35)

## **More info here: [Google](www.google.com)**

## Periodic table

![This is supposed to be an image](./table.jpg "alternate title")